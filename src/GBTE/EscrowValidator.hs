{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

-- PlutusV2
module GBTE.EscrowValidator (validator) where

import qualified    Ledger (contains)
import qualified    Ledger.Ada as Ada
import              Plutus.V1.Ledger.Value
import              Plutus.V2.Ledger.Api
import              Plutus.V2.Ledger.Contexts
import              Plutus.Script.Utils.V1.Typed.Scripts.Validators (DatumType, RedeemerType)
import              Plutus.Script.Utils.V2.Typed.Scripts (ValidatorTypes, TypedValidator, mkTypedValidator, mkTypedValidatorParam, validatorScript, mkUntypedValidator)
import              PlutusTx
import              PlutusTx.Prelude    hiding (Semigroup (..), unless)

import              GBTE.Types

{-# INLINEABLE escrowValidator #-}
escrowValidator :: BountyParam -> BountyEscrowDatum -> BountyAction -> ScriptContext -> Bool
escrowValidator bp dat action ctx =
  case action of
    Cancel      ->  traceIfFalse "Only Issuer can Cancel Bounty"                signedByIssuer &&
                    traceIfFalse "Can only cancel bounty after deadline"        deadlineReached
    Update      ->  traceIfFalse "Only Issuer can Update Bounty"                signedByIssuer &&
                    traceIfFalse "Update must create one new Bounty UTXO"       createsContinuingBounty &&
                    traceIfFalse "Output UTXO value must be geq datum specs"    outputFulfillsValue
    Distribute  ->  traceIfFalse "Issuer must sign to distribute bounty"        signedByIssuer &&
                    traceIfFalse "Contributor must receive full bounty values"  outputFulfillsBounty
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    bCursym :: CurrencySymbol
    bCursym = bountyTokenPolicyId bp

    bTokenN :: TokenName
    bTokenN = bountyTokenName bp

    signedByIssuer :: Bool
    signedByIssuer = txSignedBy info $ bedIssuerPkh dat

    deadlineReached :: Bool
    deadlineReached = Ledger.contains (from $ bedExpirationTime dat) $ txInfoValidRange info

    -- Update means that a UTXO must be left at contract address
    outputsToContract :: [TxOut]
    outputsToContract = getContinuingOutputs ctx

    createsContinuingBounty :: Bool
    createsContinuingBounty = length outputsToContract == 1

    outputContainsValue :: [TxOut] -> Bool
    outputContainsValue [x]   = valueOf (txOutValue x) bCursym bTokenN >= bedTokenAmount dat &&
                                Ada.getLovelace (Ada.fromValue $ txOutValue x) >= bedLovelaceAmount dat
    outputContainsValue _     = False

    outputFulfillsValue :: Bool
    outputFulfillsValue = outputContainsValue outputsToContract

    valueToContributor :: Value
    valueToContributor = valuePaidTo info $ bedContributorPkh dat

    -- The value sent to Contributor must be at least the amount specified by bounty
    -- contributor must get tokenAmount bp of gimbals and lovelaceAmount bp...
    outputFulfillsBounty :: Bool
    outputFulfillsBounty = valueOf valueToContributor bCursym bTokenN >= bedTokenAmount dat &&
                           Ada.getLovelace (Ada.fromValue valueToContributor) >= bedLovelaceAmount dat

    ownInputVal :: Value
    ownInputVal = case findOwnInput ctx of
                Just iv -> txOutValue $ txInInfoResolved iv
                Nothing -> error ()

typedValidator :: BountyParam -> TypedValidator EscrowTypes
typedValidator bp = go bp where
    go = mkTypedValidatorParam @EscrowTypes
        $$(PlutusTx.compile [|| escrowValidator ||])
        $$(PlutusTx.compile [|| wrap ||])
    wrap = mkUntypedValidator

validator :: BountyParam -> Validator
validator = validatorScript . typedValidator